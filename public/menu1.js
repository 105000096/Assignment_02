var video1;
var nametext = '';
var startLabel;
var menu1State = { 
    create: function() {
    // Add a background image 
    game.add.image(0, 0, 'backgroundmenu'); 
    video1 = game.add.video('backvideo');
    video1.play(true);
    video1.addToWorld(game.width/2, game.height/2, 0.5, 0.5, 1.1, 1.6);
    video1.volume = 0.2;
    /*backgroundmusic = game.add.audio('start');
    backgroundmusic.play();
    backgroundmusic.volume = 0.1;*/
    // Display the name of the game 
    var nameLabel = game.add.text(game.width/2, 80, 'Conan forever', { font: '50px Arial', fill: '#C0C0C0' }); 
    nameLabel.anchor.setTo(0.5, 0.5);
    // Show the score at the center of the screen 
    //var scoreLabel = game.add.text(game.width/2, game.height/2, 'score: ' + dead/5, { font: '25px Arial', fill: '#C0C0C0' }); 
    //scoreLabel.anchor.setTo(0.5, 0.5);
    // Explain how to start the game 
    if(nametext == ''){ startLabel = game.add.text(game.width/2, game.height-80, 'key in your name and press Enter: '+ nametext, { font: '25px Arial', fill: '#C0C0C0' });
game.input.keyboard.addCallbacks(this, null, null, this.keyPress);
}
    else{
        startLabel = game.add.text(game.width/2, game.height-80, nametext + ' press Up ', { font: '25px Arial', fill: '#C0C0C0' });
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP); 
        upKey.onDown.add(this.start, this);
    }
    startLabel.anchor.setTo(0.5, 0.5); 
    // Create a new Phaser keyboard variable: the up arrow key 
    // When pressed, call the 'start'
    }, 
    keyPress(char) {
        if ((char >= 'a' && char <= 'z') || (char >= 'A' && char <= 'Z')){
            nametext += char;
            startLabel.setText('key in your name and press Enter: '+ nametext);
        }
        else{
            this.start();
        }
    },
    start: function() { 
        // Start the actual game 
        //backgroundmusic.stop();
        video1.destroy();
        game.state.start('menu2');
    }, 
}; 