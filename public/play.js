var dead = 0;
var count = 0;
var explode = 0;
var onrotate = 0;
var onmouriran = 0;
var text;
var health = 100;
var backgroundmusic;
var conanmusic;
var conanheal = 0;
var spacedown = 0;
var ifla = 0;
var playState = {
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'background');
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.physics.setBoundsToWorld();
        game.renderer.renderSession.roundPixels = true;
        style = {fill : "#fff", align : "right"};
        text = game.add.text(540, 20, "score : " + dead/5 + "\nhealth : " + health + "\nheal : " + conanheal, style);

        backgroundmusic = game.add.audio('conan');
        backgroundmusic.play();
        backgroundmusic.volume = 0.1;
        conanmusic = game.add.audio('onlytruth');

        this.cursor = game.input.keyboard.createCursorKeys();


        this.player = game.add.sprite(350, 10, 'player');
        this.player.facingLeft = false;
        this.player.anchor.setTo(0.5, 0.5);
        /// ToDo: Add 4 animations.
        /// 1. Create the 'rightwalk' animation by looping the frames 1 and 2
        this.player.animations.add('rightwalk', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 8,  true);
        /// 2. Create the 'leftwalk' animation by looping the frames 3 and 4
        this.player.animations.add('leftwalk', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 8,  true);
        /// 3. Create the 'rightjump' animation (frames 5 and 6 and no loop)
        this.player.animations.add('rightjump', [5, 6], 8,  true);
        /// 4. Create the 'leftjump' animation (frames 7 and 8 and no loop)
        this.player.animations.add('leftjump', [5, 6], 8,  true);
        ///


        /// Add a little yellow block :)
        game.physics.arcade.enable(this.player, false);
        this.player.body.gravity.y = 800;
        
        this.player.body.fixedRotation = true;
        this.yellowBlock = game.add.group();
        this.yellowBlock.physicsBodyType = Phaser.Physics.ARCADE;
        this.yellowBlock.enableBody = true;
        this.blueBlock = game.add.group();
        this.blueBlock.physicsBodyType = Phaser.Physics.ARCADE;
        this.blueBlock.enableBody = true;
        this.rotateBlock = game.add.group();
        this.rotateBlock.physicsBodyType = Phaser.Physics.ARCADE;
        this.rotateBlock.enableBody = true;
        this.mouriranBlock = game.add.group();
        this.mouriranBlock.physicsBodyType = Phaser.Physics.ARCADE;
        this.mouriranBlock.enableBody = true; 
        var gen = game.rnd.between(3,4);
        var i = 180;
        var ge = gen;
        while(gen>0){
            var gen1 = game.rnd.between(i+40, i + game.height*0.66/(ge-1)-40);
            var gen2 = game.rnd.between(40, game.width-40);
            if(i == 180){this.generateblock(0, this.player.x, 140); i = i + 1}
            else{
                this.generateblock(0, gen2, gen1);
                i = i + game.height*0.66/(ge-1);
            }
            gen--;
        }        

        /// Particle
        /// Add floor
        //this.floor = game.add.sprite(0, game.height - 30, 'ground'); 
        //game.physics.arcade.enable(this.floor, false);
        //this.floor.body.kinematic = true;
        //this.floor.body.immovable = true;
        //game.physics.arcade.enable(this.player);
        // Add vertical gravity to the player
    },
    /*blockTween: function() {

        /// ToDo: Tween yellow block.
        ///      Move block to 20px above its original place and move it back (yoyo function).
        var yellowBlockOriginalX = this.yellowBlock.x;
        var yellowBlockOriginalY = this.yellowBlock.y;
        /// Add Tween here: game.add.tween(this.yellowBlock)....? 
        //game.add.tween(this.yellowBlock).to({y: yellowBlockOriginalY - 20}, 800).easing(Phaser.Easing.Bounce.Out).yoyo(true).start();
        ///
    },*/
    generateblock: function(type, x, y) {
        if(type == 0){
            var block = this.yellowBlock.create(x, y, 'block1');
            game.physics.arcade.enable(block);
            block.anchor.setTo(0.5, 0.5);
            block.body.setCircle(30, 10, 0);
            block.body.immovable = true;
            block.body.velocity.y = -80-dead;
            block.outOfBoundsKill = true;
        }
        else if(type == 1){
            var block = this.blueBlock.create(x, y, 'block2');
            game.physics.arcade.enable(block);
            block.anchor.setTo(0.5, 0.5);
            block.animations.add('Bblockanim', [0, 1, 2, 3, 4, 5, 6], 10, true, true);
            block.body.setCircle(30, 20, 15);
            block.body.immovable = true;
            block.body.velocity.y = -80-dead;
            block.outOfBoundsKill = true;
        }
        else if(type == 2){
            var block = this.rotateBlock.create(x, y, 'block0');
            game.physics.arcade.enable(block);
            block.anchor.setTo(0.5, 0.5);
            block.animations.add('Rblockanim', [9, 8, 7, 6, 5, 4, 3, 2, 1, 0], 20, true, true);
            block.body.setCircle(50, 0, 0);
            block.body.immovable = true;
            block.body.velocity.y = -80-dead;
            block.outOfBoundsKill = true;
            block.animations.play('Rblockanim');
        }
        else{
            var block = this.mouriranBlock.create(x, y, 'block3');
            game.physics.arcade.enable(block);
            block.anchor.setTo(0.5, 0.5);
            block.body.immovable = true;
            block.body.velocity.y = -80-dead;
            block.outOfBoundsKill = true;
        }
    },
    blockParticle: function() {

        /// ToDo: Start our emitter.
        ///      1. We'll burst out all partice at once.
        ///      2. The particle's lifespan is 800 ms.
        ///      3. Set frequency to null since we will burst out all partice at once.
        ///      4. We'll launch 15 particle.
        this.emitter = game.add.emitter(200, 200, 200);
        this.emitter.makeParticles('Conan');
        //this.emitter.setYSpeed(-150, 150);
        //this.emitter.setXSpeed(-150, 150);
        //this.emitter.setScale(2, 0, 2, 0, 800);
        //this.emitter.gravity = 500;
        this.emitter.start(true, 5000, 20);
        ///
    },
    update: function() {
        //game.physics.arcade.collide(this.player, this.walls);
        //game.physics.arcade.collide(this.player, this.floor);
        if(game.physics.arcade.collide(this.player, this.mouriranBlock, null, null, this)){
            this.mouriranBlock.forEachAlive(function(child){
                if(onmouriran == 0) {
                    onmouriran = 1;
                    health -= 10;
                    this.blockParticle();
                }
            }, this);
        }
        else{
            onmouriran = 0;
        }
        game.physics.arcade.collide(this.player, this.yellowBlock, null, null, this);
        if(game.physics.arcade.collide(this.player, this.rotateBlock, null, null, this)){
            if(onrotate == 0){this.player.body.velocity.x = this.player.body.velocity.x + 800;}
            onrotate = 1;
        }
        else{
            if(onrotate>=1 && onrotate<= 50){onrotate++;}
            onrotate = 0;
        }
        if(game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)){
            if(spacedown == 0){
                spacedown = 1;
                if(conanheal>0) {conanmusic.play(); conanheal --; health = 100; ifla++;}
            }
        }
        else{
            spacedown = 0;
        }
        if(game.physics.arcade.collide(this.player, this.blueBlock, null, null, this)){
            this.blueBlock.forEachAlive(function(child){
                if(game.physics.arcade.collide(this.player, child, null, null, this)){
                    explode++;
                    if(explode>50){
                        child.animations.play('Bblockanim', 10, true);
                    }
                    if(explode>80){
                        var i = 0;
                        child.kill();
                        explode = 0;
                    }
                }
            }, this);    
        }

        if(dead%81 == 27) {
            backgroundmusic.stop();
            backgroundmusic = game.add.audio('lan');
            backgroundmusic.play();
            backgroundmusic.volume = 0.1;
        }
        else if(dead%81 == 54) {
            backgroundmusic.stop();
            backgroundmusic = game.add.audio('rotate');
            backgroundmusic.play();
            backgroundmusic.volume = 0.1;
        }
        else if(dead%81 == 80){
            backgroundmusic.stop();
            backgroundmusic = game.add.audio('conan');
            backgroundmusic.play();
            backgroundmusic.volume = 0.1;
        }
        count++;


        if (!this.player.inWorld||health <= 0) {
            backgroundmusic.stop();
            count = 0;explode = 0;onrotate = 0;onmouriran = 0;conanheal = 0;spacedown = 0;ifla = 0;
            this.playerDie();
        }
        this.movePlayer();
        this.yellowBlock.forEachAlive(function(child){
            if(child.body.y<-80){
                child.kill();
            }
        }, this);
        this.blueBlock.forEachAlive(function(child){
            if(child.body.y<-80){
                child.kill();
            }
        }, this);
        this.rotateBlock.forEachAlive(function(child){
            if(child.body.y<-80){
                child.kill();
            }
        }, this);
        this.mouriranBlock.forEachAlive(function(child){
            if(child.body.y<-80){
                child.kill();
            }
        }, this);
        if(this.yellowBlock.countDead() + this.blueBlock.countDead() + this.rotateBlock.countDead() + this.mouriranBlock.countDead()>dead){
            var gen2 = game.rnd.between(70,90);
            var gen = game.rnd.between(40,game.width-40);
            var gen3 = game.rnd.between(1,10);
            if(gen3 == 1){
                gen3 = 1;
            }
            else if(gen3 >= 2 && gen3 <= 3){
                gen3 = 2;
            }
            else if(gen3 >= 4 && gen3 <= 6){
                gen3 = 3;
            }
            else{
                gen3 = 0;
            }
            if(count > gen2){
                this.generateblock(gen3, gen, game.height+40);
                count = 0;
                dead++;
            }
        }
        conanheal = Math.floor(dead/30) - ifla;
        text.setText("score : " + dead/5 + "\nhealth : " + health + "\nheal : " + conanheal);
    }, 
    playerDie: function() {
        health = 100;
        game.state.start('menu1');
    },

    /// ToDo: Finish the 4 animation part.
    movePlayer: function() {
        if (this.cursor.left.isDown) {
            if(!this.player.facingLeft) {
                this.player.scale.x *= -1;
            }
            this.player.body.velocity.x = -250;
            this.player.facingLeft = true;

            /// 1. Play the animation 'leftwalk'
            this.player.animations.play('leftwalk');
            ///
        }
        else if (this.cursor.right.isDown) { 
            if(this.player.facingLeft) {
                this.player.scale.x *= -1;
            }
            this.player.body.velocity.x = 250;
            this.player.facingLeft = false;

            /// 2. Play the animation 'rightwalk' 
            this.player.animations.play('rightwalk');
            ///
        }    

        // If the up arrow key is pressed, And the player is on the ground.
        else if (this.cursor.up.isDown) { 
            if(game.physics.arcade.collide(this.player, this.yellowBlock)||onmouriran||game.physics.arcade.collide(this.player, this.blueBlock)||game.physics.arcade.collide(this.player, this.rotateBlock)){
                // Move the player upward (jump)
                if(this.player.facingLeft) {
                    /// 3. Play the 'leftjump' animation
                    this.player.animations.play('leftjump');
                    ///
                }else {
                    /// 4. Play the 'rightjump' animation
                    this.player.animations.play('rightjump');
                    ///
                }
                this.player.body.velocity.y = -300;
            }
        }  
        // If neither the right or left arrow key is pressed
        else if (onrotate == 0) {
            // Stop the player 
            this.player.body.velocity.x = 0;
        
            if(this.player.facingLeft) {
                // Change player frame to 3 (Facing left)
                this.player.frame = 0;
            }else {
                // Change player frame to 1 (Facing right)
                this.player.frame = 0;
            }

            // Stop the animation
            this.player.animations.stop();
        }    
    }
};