var loadState = { 
    preload: function () {
    // Add a 'loading...' label on the screen 
    var loadingLabel = game.add.text(game.width/2, 150, 'loading...', { font: '30px Arial', fill: '#ffffff' }); 
    loadingLabel.anchor.setTo(0.5, 0.5);
    // Display the progress bar 
    var progressBar = game.add.sprite(game.width/2, 200, 'progressBar'); 
    progressBar.anchor.setTo(0.5, 0.5); game.load.setPreloadSprite(progressBar);
    // Load all game assets 
    // Loat game sprites.
    game.load.image('background', 'assets/background.jpg');
    //game.load.image('ground', 'assets/ground.png');
    game.load.image('pixel', 'assets/pixel.png');
    game.load.image('Control', 'assets/Control.png');
    
    /// Load block spritesheet.
    game.load.spritesheet('block0', 'assets/block0.png', 100, 100);
    game.load.spritesheet('block1', 'assets/block1.png', 120, 120);
    game.load.spritesheet('block2', 'assets/block2.png', 100, 120);
    game.load.spritesheet('block3', 'assets/block3.png', 120, 21);
    game.load.spritesheet('button', 'assets/button.png', 200, 60);
    
    /// Load audio
    game.load.audio('conan', 'assets/back.mp3');
    game.load.audio('lan', 'assets/lan.mp3');
    game.load.audio('rotate', 'assets/rotate.wav');
    game.load.audio('onlytruth', 'assets/onlytruth.mp3');
    game.load.audio('start', 'assets/menu.mp3');
    game.load.audio('conanmax', 'assets/backtoo.mp3');
    /// Load video
    game.load.video('backvideo', 'assets/conan.Mp4');

    game.load.spritesheet('player', 'assets/Conan.png', 40, 65);
    ///
    // Load a new asset that we will use in the menu state 
    game.load.image('backgroundmenu', 'assets/menubackground.jpg');
    }, create: function() { 
        // Go to the menu state 
        game.state.start('menu1'); 
    } 
}