var gameState = {

    width: 692,
    height: 558,
    renderer: Phaser.AUTO,
    parent: 'canvas',       // inject into 'canvas' in index.html
    transparent: false,      // non-transparent background
    antilias: true,         // smooth graphics

}

var game = new Phaser.Game(gameState);
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu1', menu1State);
game.state.add('menu2', menu2State);
game.state.add('play', playState);
game.state.add('cheat', cheatState);
game.state.start('boot');



