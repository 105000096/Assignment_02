# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |          2h
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |           100
|         All things in your game should have correct physical properties and behaviors.         |  15%  |          100
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |          100
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |          100
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |2h
| Appearance (subjective)                                                                        |  10%  |          100
| Other creative features in your game (describe on README.md)                                   |  10%  |          100

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed
遊戲基本介紹：
1.本遊戲為柯南下樓梯，loading界面加載完后會進入一段動畫
（因為剪的太好所以一定想加上去XD），然後如果輸入自己的遊戲名字，遊戲的分數便會被存入firebase的database。
2.銀色子彈（註：柯南在動漫中有時被稱為銀色子彈）有幾個選項，start為正常玩法，cheat為作弊模式，contr為
操控按鍵，score為分數（可以存用戶名字和分數，但真的沒時間做leaderboard的顯示了QQ）。
3.遊戲有四種平台，一個是足球（註：動漫柯南經常使用足球腰帶產生足球），一個是旋轉的轉盤（註：取自柯南經典音樂
《轉動的命運之輪》封面輪盤）會讓站上的人向右加速，一個是氣泡站久會爆炸，一個是尖刺（註：仔細看是小蘭頭上的角XD）會使人受傷。
4.玩家在一段時間后會獲得heal技能點數，使用可以回復滿生命力。
額外 creative features：
1.遊戲會隨時間速度越來越快。
2.4種不同功能平台。
3.技能heal。
4.開頭動畫製作（剪輯加工網上原有視頻）。
5.2種不同遊戲模式。
6.每個場景不同背景音樂，且遊戲中會根據層數變化改變背景音樂。
7.銀色子彈，足球平台，轉盤，小蘭頭髮組成的尖刺平台都取自動漫比較經典的梗。